<div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>">
<?php if ($comment->new) : ?>
  <a id="new"></a>
  <span class="new"><?php print $new ?></span>
<?php endif; ?>

<?php
 $name = $comment->name;
?>

<div class="title"><?php print $title ?></div>
    <?php if ($picture) { ?>
      <?php print $picture ?>
    <?php }; ?>
  <div class="author"><?php print $submitted ?></div>
  <div class="content"><?php print $content ?></div>
  <?php if ($picture) : ?>
    <br class="clear" />
  <?php endif; ?>
  <div class="links"><?php print $links ?></div>
</div>
