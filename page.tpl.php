<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<?php print $language ?>" xml:lang="<?php print $language ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title><?php print $head_title ?></title>
   <?php 
    print $head;
    print theme('stylesheet_import', base_path() . path_to_theme() ."/modules.css");
    print theme('stylesheet_import', base_path() . path_to_theme() ."/nav.css");
    print theme('stylesheet_import', base_path() . path_to_theme() ."/layout.css");
    print $styles;
  ?>
 <!--[if lte IE 6]>
 <style type="text/css" media="screen">
   /* IE min-width trick */
   div#wrapper1 { 
     width:expression(((document.compatMode && document.compatMode=='CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth) < 720 ? "720px" : "auto"); 
     }
  </style>
  <![endif]-->
</head>
<body <?php print theme("onload_attribute"); ?>>
<div class="hide">
 <a href="#content" title="Skip the site navigation to go directly to the content">Skip to content</a>
</div>
<!-- closes #header-->
<!-- START: HEADER -->
		
<div id="wrapper1">
 <div id="wrapper2">
  <div class="header" id="header">
   <div class="headerpadding">
     <?php if ($logo) : ?>
      <div id="site-name"><a href="<?php print $base_path ?>" title="Home"><img
       src="<?php print $logo ?>" alt="<?php print $site_name ?> Logo" /></a></div>
     <?php endif; ?>
		
		
		<span id="site-name-shadow">[<a href="<?php print $base_path ?>"><?php print $site_name ?></a>]</span>
		<!--span id="site-name-shadow">[<?php print $site_name ?>]</span-->
		 <?php if ($site_slogan) : ?>
  	 			 <div id="site-slogan"><span><?php print($site_slogan) ?></span></div>
		 <?php endif;?>
		
    <div id ="region_header"><?php print $header ?></div>
    
    <?php if (module_exist('banner')) : ?>
     <div id="banner"><?php print banner_display() ?></div>
    <?php endif; ?>

    <?php if ($primary_links) : ?>
     <ul id="primary">
      <?php foreach (array_reverse($primary_links) as $link): ?>
       <li><?php print $link; ?></li>
      <?php endforeach; ?>
     </ul>

    <?php elseif ($secondary_links) : ?>
     <ul id="secondary">
      <?php foreach (array_reverse($secondary_links) as $link): ?>
       <li><?php print $link; ?></li>
      <?php endforeach; ?>
     </ul>
    <?php endif; ?>

    <?php if ($search_box) : ?>
     <?php print $search_box ?> 
    <?php endif; ?>   
      
    </div>
   </div>
<!-- END: HEADER-->
<hr class="hide" />  
    <div class="columns">
      <?php if ($sidebar_left != '') { ?>
      <div class="leftcolumn sidebar" id="sidebar-left">
        <div class="leftpadding">
            <?php print $sidebar_left; ?>
          </div>
        </div>
      <?php } ?>
      <?php if ($sidebar_right != '') { ?>
      <div class="rightcolumn sidebar" id="sidebar-right">
        <div class="rightpadding">
          <?php print $sidebar_right; ?>
        </div>
      </div>
      <?php } ?>
      <hr class="hide" />  
      <div class="centercolumn">
        <div class="centerpadding">
          <div class="main-content" id="main">
            <?php if (($_GET['q']) != variable_get('site_frontpage','node')): ?>

              <?php if ($breadcrumb != ""): ?>
                <div id="breadcrumbs">
                  <?php print $breadcrumb;?> <span class="breadcrumb">  &raquo;  <?php if ($title != ""): print ucwords($title); endif; ?></span>
                </div>
              <?php endif; ?>
            <?php endif; ?>
            <?php if ($mission != ""): ?>
              <div id="mission"><span id="mission-text"><?php print $mission ?></span></div>
            <?php endif; ?>
            <?php if ($title != ""): ?>
              <h1 id="title"><?php print $title ?></h1>
            <?php endif; ?>
            <?php if ($messages != ""): ?>
              <div id="message"><?php print $messages ?></div>
            <?php endif; ?>
            <?php if ($help != ""): ?>
              <div id="help"><?php print $help ?></div>
            <?php endif; ?>
	    <?php if ($tabs != ""): ?>
		<?php print $tabs ?>
            <?php endif; ?>

            <!-- start main content -->
	    <span style="width: 100%;"><a name="content"></a></span>
            <?php print($content) ?>
            <!-- end main content -->
          </div><!-- main -->
        </div>
      </div>
    </div>
    <div class="clearing"></div>
    <hr class="hide" />
<!-- START: FOOTER-->
    <div id="footer" class="footer"><p>
      <?php if ($footer_message) : ?>
        <?php print $footer_message;?>
      <?php endif; ?> </p><br/><p>Designed by <a href="http://www.signfuse.com">SignFuse - Sign Language Media</a></p>
      <?php print $closure;?>
    </div>
<!-- END: FOOTER -->
	</div><!-- wrapper -->
</div><!-- outer_wrapper -->
</body>
</html>

